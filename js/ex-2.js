/**
 * 
 * Đầu vào là khu vực ưu tiên, đối tượng ưu tiên , điểm chuẩn và điểm 3 môn 
 * 
 * 
 * Xử lý : 
 * DOM tới tất cả các ô input mà user nhập , ép kiểu sang number bằng cách * 1 ; 
 * xây dựng hàm khu vực , hàm đối tượng để cộng ưu tiên 
 * cộng dồn các điểm ưu tiên vào điểm tổng, sau đó so sánh với điểm chuẩn 
 * 
 */



function TinhDiemUuTienKhuVuc(khuvuc) {
    if (khuvuc == "A") {
        return 2
    }
    if (khuvuc == "B") {
        return 1
    }
    if (khuvuc == "C") {
        return 0.5
    }
}

function TinhDiemUuTienDoiTuong(doituong) {
    if (doituong == "1") {
        return 2.5
    }
    if (doituong == "2") {
        return 1.5
    }
    if(doituong == "3") {
        return 1
    }
}

document.getElementById("tinhdiem").addEventListener('click', function(){
    var diemMon1 = document.getElementById('diem1').value * 1 ;
    var diemMon2 = document.getElementById('diem2').value * 1 ; 
    var diemMon3 = document.getElementById('diem3').value * 1 ; 
    var diemTong = diemMon1 + diemMon2 + diemMon3 ; 
    var diemChuan = document.getElementById('diemchuan').value * 1 ; 
    var khuvucEl = document.getElementById('khuvuc').value ; 
    var doiuongEl = document.getElementById('doituong').value ; 
    // Xét điều kiện khu vực 
    if (khuvucEl == "A") {
        diemTong = diemTong + TinhDiemUuTienKhuVuc(khuvucEl) ; 
    }else if (khuvucEl == "B") {
        diemTong = diemTong + TinhDiemUuTienKhuVuc(khuvucEl) ; 
    }else if (khuvucEl == "C") {
        diemTong = diemTong + TinhDiemUuTienKhuVuc(khuvucEl) ; 
    }else {
        diemTong
    }
    // Xét điều kiện đối tượng 
    if (doiuongEl == "1") {
        diemTong = diemTong + TinhDiemUuTienDoiTuong(doiuongEl) ; 
    }else if (doiuongEl == "2") {
        diemTong = diemTong + TinhDiemUuTienDoiTuong(doiuongEl) ; 
    }else if (doiuongEl == "3") {
        diemTong = diemTong + TinhDiemUuTienDoiTuong(doiuongEl) ; 
    }else {
        diemTong
    }
    // So sánh với điểm chuẩn 
    if (diemMon1 > 0 && diemMon2 > 0 && diemMon3 > 0) {
        if (diemTong >= diemChuan) {
            document.getElementById('show-ketqua1').innerHTML = `Tổng điểm của bạn là ${diemTong}, chúc mừng bạn đã đỗ`
        }else {
            document.getElementById('show-ketqua1').innerHTML = `Tổng điểm của bạn là ${diemTong} , bạn không đủ điểm để đỗ`; 
        }
    }else {
        document.getElementById('show-ketqua1').innerHTML = `Bạn không đủ điều kiện đỗ vì có 1 môn điểm 0` ; 
    }
})