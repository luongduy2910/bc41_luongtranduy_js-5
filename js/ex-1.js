/**
 * Dữ liệu đầu vào là tên người dùng và số kw 
 * 
 * Xử lý : 
 * DOM tới 2 ô input mà user nhập, ô kw điện ép kiểu sang number bằng cách * 1 
 * Xây dựng các hàm để tính tiền điện 50Kw đầu, 50Kw tiếp theo, 100Kw tiếp theo , 150 Kw tiếp theo 
 * Sử dụng if.... else if.... else để xử lý tiền điện 
 * 
 * 
 */




function TinhTienDien50KwDauTien(kw) {
    if (kw <= 50) {
        return kw *500 ; 
    }
}

function TinhTienDien50KwTiepTheo(kw) {
    if (kw <= 100) {
        return 50*500 + (kw -50) * 650 ; 
    }
}

function TinhTienDien100KwTiepTheo(kw) {
    if (kw <= 200) {
        return 50*500 + 50 * 650 + (kw - 100) * 850 ; 
    }
}

function TinhTienDien150KwTiepTheo(kw) {
    if (kw <= 350) {
        return 50*500 + 50 * 650 + 100 * 850 + (kw - 200) * 1100 ; 
    }
}

function TinhTienDien350KwTiepTheo(kw) {
    if (kw > 350) {
        return 50*500 + 50 * 650 + 100 * 850 + 150 * 1100 + (kw -350) * 1300 ; 
    }
}

document.getElementById('tinhtiendien').addEventListener('click' , function() {
    var tienDien ; 
    var KwUserEl = document.getElementById('kw').value * 1 ; 
    var nameUserEl = document.getElementById('hovaten').value ; 
    if(KwUserEl <=  50) {
        tienDien = new Intl.NumberFormat('vn-Vn').format(TinhTienDien50KwDauTien(KwUserEl)) ; 
        document.getElementById('show-ketqua').innerHTML = `Họ và tên: ${nameUserEl} , tiền điện tháng này là ${tienDien}`
    }else if (KwUserEl <= 100) {
        tienDien = new Intl.NumberFormat('vn-Vn').format(TinhTienDien50KwTiepTheo(KwUserEl)); 
        document.getElementById('show-ketqua').innerHTML = `Họ và tên: ${nameUserEl} , tiền điện tháng này là ${tienDien}`

    }else if (KwUserEl <= 200) {
        tienDien = new Intl.NumberFormat('vn-Vn').format(TinhTienDien100KwTiepTheo(KwUserEl)) ; 
        document.getElementById('show-ketqua').innerHTML = `Họ và tên: ${nameUserEl} , tiền điện tháng này là ${tienDien}`
    }else if (KwUserEl <= 350) {
        tienDien = new Intl.NumberFormat('vn-Vn').format(TinhTienDien150KwTiepTheo(KwUserEl)) ; 
        document.getElementById('show-ketqua').innerHTML = `Họ và tên: ${nameUserEl} , tiền điện tháng này là ${tienDien}`
    }else {
        tienDien = new Intl.NumberFormat('vn-Vn').format(TinhTienDien350KwTiepTheo(KwUserEl)) ; 
        document.getElementById('show-ketqua').innerHTML = `Họ và tên: ${nameUserEl} , tiền điện tháng này là ${tienDien}`
    }
})





